/* eslint-disable no-undef */
let boardSize = 3;
let turnSelector = false;

const generateBoard = () => {
  const newBoard = document.createElement("div");

  for(let i = 0; i < boardSize; i++) {
    const row = document.createElement("div");
    row.className = "board-row";
    row.id = `r${i}`;

    for (let j = 0; j < boardSize; j++) {
      const tile = document.createElement("div");
      const tileId = `t${i}${j}`;
      tile.className = "board-tile";
      tile.id = tileId;
      tile.onclick = () => turnClickHandler(tileId);
      row.appendChild(tile);
    }
    newBoard.appendChild(row);
  }
  document.querySelector("#tictacBoard").replaceChildren(newBoard);
};

const turnClickHandler = tileId => {
  const tile = document.querySelector(`#${tileId}`);
  if (!tile.childNodes.length) {
    tile.innerText = turnSelector ? "O" : "X";
    turnSelector = !turnSelector;
  }
};

// eslint-disable-next-line no-unused-vars
const resetBoard = () => {
  generateBoard();
};

// eslint-disable-next-line no-unused-vars
const changeBoardSize = bool => {
  if (bool) {
    boardSize++;
    return generateBoard();
  }

  if (boardSize > 3) {
    boardSize--;
    return generateBoard();
  }
};

generateBoard();