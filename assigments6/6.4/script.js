/* eslint-disable no-undef */
//the new paragraphs are under a single <p> tag, but that's fine for this.
const paragrapher = () => {
  const p = document.querySelector("p");
  const bySentences = p.textContent.split(".");
  p.innerText = "";

  for (const sentence of bySentences) {
    const words = sentence.split(" ").reduce((prev, current) => {
      return current.length > 5
        ? `${prev} <mark>${current}</mark> `
        : `${prev} ${current} `;
    }, "");
    const newP = document.createElement("p");
    newP.innerHTML = words;
    p.appendChild(newP);
  }
};

paragrapher();