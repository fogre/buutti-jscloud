/* eslint-disable no-undef */
const input = document.querySelector("#todoInput");
let todoIndex = 0;

input.addEventListener("keypress", event => {
  if (event.key === "Enter" && input.value) {
    const ul =  document.querySelector("#todoList");
    const li = document.createElement("li");
    const todoId = `todo${todoIndex}`;
    li.id = todoId;
    li.onclick = () => markDone(todoId);
    li.innerText = input.value;
    ul.appendChild(li);
    todoIndex++;
  }
});

const markDone = id => {
  const todo = document.querySelector(`#${id}`);
  todo.classList.contains("checked")
    ? todo.classList.remove("checked")
    : todo.classList.add("checked");
};

// eslint-disable-next-line no-unused-vars
const removeDone = () => {
  const ul = document.querySelector("#todoList");
  ul.childNodes.forEach(todo => {
    if (todo.classList.contains("checked")) {
      ul.removeChild(todo);
    }
  });
};