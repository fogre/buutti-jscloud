/* eslint-disable no-undef */

// eslint-disable-next-line no-unused-vars
const analyzeText = () => {
  let wordCount = 0;
  let averageLength = 0;
  const allWords = {};
  const text = document.querySelector("#textInput").value;
  const words = text.split(" ");

  let sum = 0;
  words.forEach(word => {
    sum += word.length;
    allWords[word]
      ? allWords[word].count++
      : allWords[word] = { count: 0 };
  });
  wordCount = words.length;
  averageLength = sum / wordCount;
  allWords.sort((a, b) => b.count - a.count);
  showAnalised(allWords, wordCount, averageLength);
};

const showAnalised = (allWords, wordCount, averageLength) => {
  const resultNode = document.querySelector("#analysedResult");
  resultNode.replaceChildren();
  resultNode.insertAdjacentHTML("beforeend", `<p>Word count: ${wordCount}</p>`);
  resultNode.insertAdjacentHTML("beforeend",`<p>Word average length: ${averageLength}</p>`);
  resultNode.insertAdjacentHTML("beforeend", "<h3>Words:</h3>");
  allWords.forEach(word => {
    resultNode.insertAdjacentHTML("beforeend", `<p>${word.word}: ${word.count}`);
  });
};
