import fs from "fs";

let txt = "";

const reader = fs.createReadStream("./joulu.txt", "utf-8");
reader.on("data", batch => {
  txt += batch.toLowerCase()
    .replaceAll("joulu", "kinkku")
    .replaceAll("lapsilla", "poroilla");
  console.log(txt);
});

reader.on("error", err => {
  console.log(err);
});

const writer = fs.createWriteStream("./joulu2.txt");
writer.write(txt, err => {
  if (err) console.log(err);
});