const checkExam = (answers, submitted) => {
  let score = 0;
  for(let i in answers) {
    if (!submitted[i]) continue;

    answers[i] === submitted[i]
      ? score += 4
      : score--;
  }
  return score > 0 ? score : 0;
};

console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""]));