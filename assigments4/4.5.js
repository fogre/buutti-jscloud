import fs from "fs";

try {
  const data = fs.readFileSync("forecast_data.json", "utf8");
  const parsedData = JSON.parse(data);
  parsedData.temperature = parsedData.temperature + 5;

  fs.writeFileSync("forecast_data.json", JSON.stringify(parsedData), "utf8");
} catch (e) {
  console.log(e);
}