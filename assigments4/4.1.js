const Ingredient = function(name, amount) {
  this.name = name;
  this.amount = amount;
  this.toString = function() {
    return `${this.name}: ${this.amount}`;
  };
};

const Recipe = function(name, ingredients, servings) {
  this.name = name;
  this.ingredients = ingredients;
  this.servings = servings;
  this.toString = function() {
    //Ingredients are not a list anymore
    return `Name: ${this.name}
      Servings: ${this.servings}
      Ingredients: ${this.ingredients.toString()} 
    `;
  };
};

const onion = new Ingredient("onion", 1);
const garlic = new Ingredient("garlic", 3);
const water = new Ingredient("water", 4);

const onionWater = new Recipe(
  "Onion Water", [onion, garlic, water], 2
);

console.log(onionWater.toString());
