//without using eval()
const calculator = (operator, a, b) => {
  switch(operator) {
  case "+":
    return a + b;
  case "-":
    return a - b;
  case "*":
    return a * b;
  case "/":
    return a / b;
  default:
    return `Unknown operator ${operator}!`;
  }
};

console.log(calculator("sadasd", 3, 5));