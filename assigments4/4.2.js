//Ingredient
const Ingredient = function(name, amount) {
  this.name = name;
  this.amount = amount;
};

Ingredient.prototype.toString = function() {
  return `${this.name}: ${this.amount}`;
};

Ingredient.prototype.scale = function(toScale) {
  this.amount = this.amount * toScale;
};

//Recipe
const Recipe = function(name, ingredients, servings) {
  this.recipeName = name;
  this.ingredients = ingredients;
  this.servings = servings;
};

Recipe.prototype.toString = function() {
  //Ingredients are not a list
  return `Name: ${this.recipeName}\nServings: ${this.servings}\nIngredients: ${this.ingredients.toString()}`;
};

Recipe.prototype.setServings = function(newServings) {
  this.ingredients.forEach(i => i.scale(newServings / this.servings));
  this.servings = newServings;
};

/*
const onion = new Ingredient("onion", 1);
const garlic = new Ingredient("garlic", 2);
const water = new Ingredient("water", 3);

const onionWater = new Recipe("Onion Water", [onion, garlic, water], 2);

console.log(onionWater.toString());

onionWater.setServings(100);
console.log(onionWater.toString());
*/

export { Recipe, Ingredient };