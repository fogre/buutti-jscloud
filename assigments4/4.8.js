const aboveAverage = numArr => {
  const sum =  numArr.reduce((prev, current) => prev + current, 0);
  const average = sum / numArr.length;
  return numArr.filter(i => i > average);
};

console.log(aboveAverage([1, 5, 9, 3]));