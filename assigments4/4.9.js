const getVowelCount = str => {
  let count = 0;
  for (let char of str) {
    if ("aeiouy".includes(char)) count++;
  }
  return count;
};

console.log(getVowelCount("abracadabra"));