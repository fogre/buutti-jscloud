import { Recipe, Ingredient } from "./4.2.js";

class HotRecipe extends Recipe {
  constructor(recipeName, ingredients, amount, heatLevel = 0) {
    super(recipeName, ingredients, amount);
    this.heatLevel = heatLevel;
  }

  toString() {
    if (this.heatLevel > 5) {
      return "Warning, hot recipe!\n" + super.toString();
    }
    return super.toString();
  }
}

const onion = new Ingredient("onion", 1);
const garlic = new Ingredient("garlic", 2);
const water = new Ingredient("water", 3);

const onionWater = new HotRecipe("Onion Water", [onion, garlic, water], 2, 0);
console.log(onionWater.toString());

onionWater.heatLevel = 6;
console.log(onionWater.toString());
