const likes = usersArr => {
  if (!usersArr || !usersArr.length) {
    console.log("likes needs an array as a parameter");
  }

  switch(usersArr.length) {
  case 0:
    return "no one likes this";
  case (1):
    return `${usersArr[0]} likes this`;
  case (2):
    return `${usersArr[0]} and ${usersArr[1]} likes this`;
  case (3):
    return `${usersArr[0]}, ${usersArr[1]} and ${usersArr[2]} likes this`;
  default:
    return `${usersArr[0]}, ${usersArr[1]} and ${usersArr.length - 2} others likes this`;
  }
};

console.log(likes([]));
console.log(likes(["John"])); // "John likes this"
console.log(likes(["Mary", "Alex"])); // "Mary and Alex like this"
console.log(likes(["John", "James", "Linda"])); // "John, James and Linda like this"
console.log(likes(["Alex", "Linda", "Mark", "Max"])); // must be "Alex, Linda and 2 others
