import Timer from "./assigmentComponents/Timer162";
import Bingo from "./assigmentComponents/Bingo163";
import FeedbackForm from "./assigmentComponents/FeedbackForm164";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Timer />
      <hr />
      <Bingo />
      <hr />
      <FeedbackForm />
    </div>
  );
}

export default App;
