import { useState, useEffect } from "react";

const Timer = () => {
  const [time, setTime] = useState(0);
  const [start, setStart] = useState(false);

  useEffect(() => {
    const timer = start && setTimeout(() => {
      setTime(time + 1);
    }, 1000);

    return () => clearTimeout(timer);
  }, [time, start]);

  const handleChangeTime = amount => {
    setTime(time + amount);
  };

  const minutes = Math.floor(time / 60);
  const seconds = time - minutes * 60;
  const timeStr = minutes ? `${minutes}:${seconds}` : seconds;

  return (
    <section>
      <h2>16.2 Timer</h2>
      <h3>{timeStr}</h3>
      <div>
        <button onClick={() => setStart(!start)}>Start/Stop</button>
        <button onClick={() => handleChangeTime(60)}>Add minute</button>
        <button onClick={() => handleChangeTime(1)}>Add second</button>
        <button onClick={() => setTime(0)}>Reset</button>
      </div>
    </section>
  );

};

export default Timer;