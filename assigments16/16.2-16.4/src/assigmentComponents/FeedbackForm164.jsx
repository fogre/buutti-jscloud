import { useState } from "react";

const FeedbackForm = () => {
  const [emailInput, setEmailInput] = useState("");
  const [nameInput, setNameInput] = useState("");
  const [textAreaInput, setTextAreaInput] = useState("");
  const [feedbackType, setFeedbackType] = useState("");

  const handleRadioInput = newVal => {
    setFeedbackType(newVal);
  };

  const handleOnChange = (event, setter) => {
    setter(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();
    console.log({ emailInput, nameInput, textAreaInput, feedbackType });
  };

  const resetForm = () => {
    setEmailInput("");
    setNameInput("");
    setTextAreaInput("");
    setFeedbackType("");
  };

  return (
    <section>
      <h2>16.4 Feedback form</h2>
      <form onSubmit={handleSubmit}>
        <fieldset>
          <legend>Enter feedback type: *</legend>
          <div>
            <input
              id="feedbackType1"
              type="radio"
              checked={feedbackType === "feedback"}
              onChange={() => handleRadioInput("feedback")}
            />
            <label htmlFor="feedbackType1">Feedback</label>
            <input
              id="feedbackType2"
              type="radio"
              checked={feedbackType === "suggestion"}
              onChange={() => handleRadioInput("suggestion")}
            />
            <label htmlFor="feedbackType2">Suggestion</label>
            <input
              id="feedbackType2"
              type="radio"
              checked={feedbackType === "Question"}
              onChange={() => handleRadioInput("Question")}
            />
            <label htmlFor="feedbackType1">Question</label>
          </div>
        </fieldset>
        <div>
          <label htmlFor="textarea">Write your feedback: *</label>
          <textarea
            id="textarea"
            value={textAreaInput}
            onChange={e => handleOnChange(e, setTextAreaInput)}
          />
        </div>
        <div>
          <label htmlFor="nameInput">Name:</label>
          <input
            id="nameInput"
            value={nameInput}
            onChange={e => handleOnChange(e, setNameInput)}
          />
        </div>
        <div>
          <label htmlFor="emailInput">Email:</label>
          <input
            id="emailInput"
            value={emailInput}
            onChange={e => handleOnChange(e, setEmailInput)}
          />
        </div>
        <button
          type="submit"
          disabled={!feedbackType || !textAreaInput}
        >Submit</button>
        <button
          type="button"
          onClick={() => resetForm()}
        >Reset</button>
      </form>
    </section>
  );
};

export default FeedbackForm;