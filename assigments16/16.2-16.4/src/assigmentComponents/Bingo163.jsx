import { useState } from "react";

const Bingo = () => {
  //Balls of steel
  const [balls, setBalls] = useState({});

  const generateNewBall = () => {
    if (Object.keys(balls).length >= 100) return;
    // eslint-disable-next-line no-constant-condition
    while (true) {
      const randomNum =  Math.ceil(Math.random() * 100);
      if (!balls[randomNum]) {
        setBalls({ ...balls, [randomNum]: randomNum });
        return false;
      }
    }
  };

  return (
    <section>
      <h2>16.3 BINGO BONGO</h2>
      {Object.values(balls).map(ball =>
        <div key={ball} style={ballStyle}>{ball}</div>
      )}
      <div>
        <button onClick={() => generateNewBall()}>+</button>
      </div>
    </section>
  );
};

const ballStyle = {
  width: "2rem",
  borderRadius: "50%",
  padding: "10px 6px",
  backgroundColor: "orange",
  marginBottom: "2px"
};

export default Bingo;