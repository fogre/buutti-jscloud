import { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [destiny, setDestiny] = useState("");

  const getDestiny = async () => {
    const url =  process.env.NODE_ENV === "production"
      ? "/destiny"
      : "http://localhost:3001/destiny";
    const res = await fetch(url);
    const dest = await res.text();
    setDestiny(dest);
  };

  useEffect(() => {
    getDestiny();
  }, []);

  return (
    <div className="App">
      <h1>Express yourself!</h1>
      <p>{destiny}</p>
    </div>
  );
}

export default App;
