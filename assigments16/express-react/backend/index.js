import express from "express";
import cors from "cors";

const app = express();
app.use(cors());

app.get("/destiny", (_req, res) => {
  res.send("You will die someday.");
});

app.use("/", express.static("dist"));

app.listen(3001, () => {
  console.log("listening");
});

export default app;