import { useState } from "react";
import ContactForm from "./components/ContactForm";
import ContactPerson from "./components/ContactPerson";
import NavColumn from "./components/NavColumn";
import "./App.css";

let contactId = 0;

const App = () => {
  const [contacts, setContacts] = useState([]);
  /*
    State to set which component should be shown in <main>
    Object with field view, which has 4 valid states: "main", "addContact", "editContact" and "contact".
    In "contact" case the mainView obj needd additional field id.
  */
  const [mainView, setMainView] = useState({ view: "main" });

  const addContact = newContact => {
    const newId = contactId++;
    setContacts(prevState => [
      ...prevState,
      { ...newContact, id: newId }
    ]);
    changeMainView({ view: "contact", id: newId });
  };

  const editContact = updated => {
    const updatedContacts = contacts.map(c => {
      //getting the id from mainView is "haxy", but works for this :D
      if (c.id === mainView.id)
        return { id: c.id, ...updated };
      return c;
    });
    setContacts(updatedContacts);
    changeMainView({ view: "contact", id: mainView.id });
  };

  const removeContact = id => {
    setContacts(contacts.filter(c => c.id !== id));
    changeMainView({ view: "main" });
  };

  const changeMainView = newView => {
    setMainView(newView);
  };

  //Component to render in <main>. Changed by mainView
  const MainViewComponent = () => {
    switch(mainView.view) {
    case "addContact":
      return (
        <>
          <h2>Add contact</h2>
          <ContactForm
            submitFunc={addContact}
            cancelFunc={() => changeMainView({ view: "main" })} />
        </>
      );
    case "editContact": {
      const person = contacts.find(p => p.id === mainView.id);
      return (
        <>
          <h2>Edit contact</h2>
          <ContactForm
            contact={person}
            submitFunc={editContact}
            cancelFunc={() => changeMainView({ view: "main" })} />
        </>
      );
    }
    case "contact": {
      const person = contacts.find(p => p.id === mainView.id);
      return <ContactPerson
        contact={person}
        removeContact={removeContact}
        changeMainView={changeMainView}
      />;
    }
    default:
      return <h1>Contact Manager</h1>;
    }
  };

  return (
    <div className="App">
      <NavColumn contacts={contacts} changeMainView={changeMainView} />
      <main>
        <MainViewComponent />
      </main>
    </div>
  );
};

export default App;
