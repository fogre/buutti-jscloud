import { useState } from "react";

const NavColumn = ({ contacts, changeMainView }) => {
  const [filter, setFilter] = useState("");

  const filteredContacts = filter
    ? contacts.filter(contact => contact.name.toLowerCase().includes(filter))
    : contacts;

  return (
    <nav>
      <input onChange={e => setFilter(e.target.value)} />
      <div>
        {filteredContacts.map(contact =>
          <div key={contact.id}>
            <button
              onClick={() => changeMainView({ view: "contact", id: contact.id })}
            >
              {contact.name}
            </button>
          </div>
        )}
      </div>
      <div>
        <button onClick={() => changeMainView({ view: "addContact" })}>
          Add Contact
        </button>
      </div>
    </nav>
  );
};

export default NavColumn;