const ContactPerson = ({ contact, removeContact, changeMainView }) => {
  const personFields = Object.entries(contact).map(([key, val]) => {
    if (key !== "name" && key !=="id") {
      return (
        <p key={key}>
          <span className="capitalize">{key}:</span> {val}
        </p>
      );
    }
  });

  return (
    <div>
      <h2>{contact.name}</h2>
      <div>
        {personFields}
      </div>
      <div>
        <button onClick={() => removeContact(contact.id)}>remove</button>
        <button
          onClick={() => changeMainView({ view: "editContact", id: contact.id })}
        >edit</button>
      </div>
    </div>
  );
};

export default ContactPerson;