//returns form input based on input type
const InputField = ({ field }) => {
  const props = { name: field.name, defaultValue: field.value };
  switch(field.type) {
  case "textarea":
    return <textarea {...props} />;
  default:
    return <input {...props} />;
  }
};

/*
  Default uncontrolled form for contacts.
  submitFunc and cancelFunc are mandatory props.
  Takes optional prop contact, which is a contact object
*/
const ContactForm = ({ contact, submitFunc, cancelFunc }) => {
  const formContact = contact ? { ...contact } : {};
  const fields = [
    { name: "name", label: "Name", value: formContact.name },
    { name: "email", label: "Email address", value: formContact.email },
    { name: "phone", label: "Phone number", value: formContact.phone },
    { name: "address", label: "Address", value: formContact.address },
    { name: "website", label: "Website", value: formContact.website },
    { name: "notes", label: "Notes", value: formContact.notes, type: "textarea" }
  ];

  const handleSubmit = event => {
    event.preventDefault();
    const submitObj = {};
    fields.forEach(field => {
      if (event.target[field.name].value) {
        submitObj[field.name] = event.target[field.name].value;
      }
    });
    submitFunc(submitObj);
  };

  return (
    <form onSubmit={handleSubmit}>
      {fields.map(field =>
        <div key={field.name}>
          <label>{field.label}</label>
          <InputField field={field}/>
        </div>
      )}
      <div>
        <button type="submit">Submit</button>
        <button type="button" onClick={cancelFunc}>Cancel</button>
      </div>
    </form>
  );
};

export default ContactForm;