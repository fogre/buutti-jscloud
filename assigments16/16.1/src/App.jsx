import { useState } from "react";
import "./App.css";

//Very ugly but works. UI beautiful though.
function App() {
  const [todos, setTodos] = useState([]);
  let todoInput = "";

  const removeTodo = index => {
    setTodos(todos.filter((_, i) => i !== index));
  };

  return (
    <div>
      <h1 className="animate">Todos</h1>
      <section className="todo-input">
        <input onChange={event => todoInput = event.target.value} />
        <button onClick={() => setTodos(todos.concat(todoInput))}>
          Add todo
        </button>
      </section>
      <section className="card animate">
        <ul className="animate">
          {todos.map((todo,i) =>
            <li key={i} className="todo-list animate">
              {todo}
              <button onClick={() => removeTodo(i)}>
                X
              </button>
            </li>
          )}
        </ul>
      </section>
    </div>
  );
}

export default App;
