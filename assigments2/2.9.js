const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

let largest, secondLargest;
for(const i of arr) {
  if (!largest || i > largest) {
    secondLargest = largest;
    largest = i;
  } else if (!secondLargest || i > secondLargest) {
    secondLargest = i;
  }
}

console.log(largest, secondLargest);