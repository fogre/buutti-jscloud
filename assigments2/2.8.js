const height = process.argv[2];

if (isNaN(height)) {
  process.exit(1);
}

for(let i = 0; i < height; i++) {
  let text = "";
  for(let j = i; j < height; j++) {
    text += " ";
  }
  for(let j = 1; j <= 2*i-1; j++) {
    text += "&";
  }
  console.log(text);
}