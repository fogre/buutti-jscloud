const [str, charToReplace, replacer] = process.argv.slice(2);

if (charToReplace) {
  console.log(str.replaceAll(charToReplace, replacer));
  process.exit(0);
}
const n = str.lastIndexOf(" ");
console.log(str.substring(0, n));