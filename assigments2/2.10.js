const students = [
  { name: "Markku", score: 99 },
  { name: "Karoliina", score: 58 },
  { name: "Susanna", score: 69 },
  { name: "Benjamin", score: 77 },
  { name: "Isak", score: 49 },
  { name: "Liisa", score: 89 },
];

let highest, lowest, average = 0;

for(const student of students) {
  const score = student.score;
  average += score;

  if (!highest || score > highest) {
    highest = student;
  }

  if (!lowest || score < lowest) {
    lowest = student;
  }

  if (score < 40)
    student.grade = 1;
  else if (score < 60)
    student.grade = 2;
  else if (score < 80)
    student.grade = 3;
  else if (score < 95)
    student.grade = 4;
  else
    student.grade = 5;
}

average = average / students.length;

for (const student of students) {
  if (student.score > average) {
    console.log(student);
  }
}



