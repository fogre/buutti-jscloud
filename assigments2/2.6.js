const n = process.argv[2];

let sum = 0;
let i = 0;

while(i <= n) {
  if (!(i % 3) || !(i % 5)) {
    sum += i;
  }
  i++;
}

console.log(sum);