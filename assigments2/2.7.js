for(let i = 1; i < 100; i++) {
  let text = "";
  if (!(i % 3)) {
    text += "Fizz";
  }
  if (!(i % 5)) {
    text += "Buzz";
  }
  text ? console.log(text) : console.log(i);
}