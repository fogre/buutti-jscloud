import express from "express";

let id = 0;
const notes = {};
const app = express();

app.use(express.json());

app.get("/", (_req, res) => {
  res.status(200).json(notes);
});

app.post("/", (req, res) => {
  notes[id] = {
    note: req.body.note,
    by: req.body.by || null
  };
  id++;
  res.status(201).json(notes[id-1]);
});

app.delete("/:id", (req, res) => {
  //check by objects own properties so it is safer to delete.
  const key = Object.keys(notes).find(k => k === req.params.id);
  if (key) {
    delete notes[req.params.id];
    return res.status(204).end();
  }
  res.status(401).json({ error: "what is going on???!" });
});

app.listen(3001, () => console.log("listening 3001"));