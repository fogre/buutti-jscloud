import express from "express";

const app = express();
app.use(express.json());

app.use("/", (_req, res) => {
  res.status(200).json({ foo: "bar" });
});

app.listen(3001, () => {
  console.log("Server running on 3001");
});
