import express from "express";
import axios from "axios";
import cors from "cors";

const app = express();
app.use(express.json());
app.use(cors());

app.use("/", async (_req, res) => {
  try {
    const apiResponse = await axios.get("http://api1:3001/");
    res.status(200).json(apiResponse.data);
  } catch(e) {
    console.log(e);
    res.status(504).json({ error: e });
  }
});

app.listen(3002, () => {
  console.log("Server running on 3002");
});
