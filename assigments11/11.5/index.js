import express from "express";

const app = express();

app.use("/", (_req, res) => {
  res.status(200).send("hello");
});

app.listen(3001, () => {
  console.log("Server running on 3001");
});
