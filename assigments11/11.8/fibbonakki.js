let current = 0, next = 1, sum;

setInterval(() => {
  console.log(current);
  sum = current + next;
  current = next;
  next = sum;
}, 1000);