import { useState } from "react";

/*Not controlled though*/
const InputHeading = () => {
  const [headingStr, setHeadingStr] = useState("");

  const handleSubmit = event => {
    event.preventDefault();
    setHeadingStr(event.target.newStr.value);
  };

  return (
    <div>
      <h2>Your str: {headingStr}</h2>
      <form onSubmit={handleSubmit}>
        <input name="newStr" />
        <button type="submit">submit</button>
      </form>
    </div>
  );
};

export default InputHeading;