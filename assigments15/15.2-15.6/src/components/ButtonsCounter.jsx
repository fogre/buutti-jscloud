import { useState } from "react";

const CounterButton = ({ count, setCount }) => {
  const handleClick = () => setCount(count+1);

  return (
    <div>
      <button onClick={(handleClick)}>
        {count}
      </button>
    </div>
  );
};

const ButtonsCounter = () => {
  const [count1, setCount1] = useState(0);
  const [count2, setCount2] = useState(0);
  const [count3, setCount3] = useState(0);

  return (
    <div>
      <CounterButton count={count1} setCount={setCount1} />
      <CounterButton count={count2} setCount={setCount2} />
      <CounterButton count={count3} setCount={setCount3} />
      <div>
        {count1 + count2 + count3}
      </div>
    </div>
  );
};

export default ButtonsCounter;