const NameList = ({ list }) => {
  const liElements = list.map((name, i) => {
    const key = `${name}-${i}`;
    return i % 2
      ? <li key={key}><i>{name}</i></li>
      : <li key={key}><b>{name}</b></li>;
  });

  return (
    <ul>
      {liElements}
    </ul>
  );
};

export default NameList;