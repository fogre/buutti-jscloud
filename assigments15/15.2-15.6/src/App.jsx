import ButtonsCounter from "./components/ButtonsCounter";
import InputHeading from "./components/InputHeading";
import NameList from "./components/NameList";

const H1 = () => <h1>Hello React!</h1>;

const H2 = () => {
  const year = new Date().getFullYear();
  return (
    <h2>The year is ONE, or really: {year}</h2>
  );
};

const App = () => {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

  return (
    <div className="App">
      <H1 />
      <H2 />
      <hr />
      <section>
        <h2>15.4</h2>
        <NameList list={namelist} />
        <hr />
      </section>
      <section>
        <h2>15.5</h2>
        <ButtonsCounter />
      </section>
      <hr />
      <section>
        <h2>15.6</h2>
        <InputHeading />
      </section>
    </div>
  );
};

export default App;
