function factorial(n) {
  if(n > 1) {
    console.log(n);
    return n * factorial(n-1);
  } else {
    return n;
  }
}

console.log(factorial(4));