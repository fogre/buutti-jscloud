import sys

bearing_input = 10
while bearing_input > -1:
  bearing_input = float(input("Bearing: "))

  if bearing_input > 360 or bearing_input < 0:
    print("Bearing input should be a number between 0-360")
    sys.exit(1)

  modded = bearing_input % 10

  number = -1
  if (modded >= 5 and bearing_input >= 100):
    number = int(str(bearing_input + 10)[:2])
  elif (bearing_input >= 100):
    number = int(str(bearing_input)[:2])
  elif (modded >= 5 and bearing_input >= 10):
    number = int(str(bearing_input + 10)[:1])
  elif(bearing_input >= 10):
    number = int(str(bearing_input)[:1])
  elif(bearing_input < 5):
    number = 0
  else:
    number = 1
    
  print(f"Runway: {number}")