import sys

word = sys.argv[1]

#Generates charIndex { 'a': 1....}
charIndex = {chr(i+96):i for i in range(1,27)}

output = ""
for char in word:
  output += str(charIndex[char])

print(output)
