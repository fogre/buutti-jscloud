competitors = ['Julia', 'Mark', 'Spencer', 'Ann' , 'John', 'Joe']
ordinals = ['st', 'nd', 'rd', 'th']

output = []
for i in range(0, len(competitors)):
  if i < 3:
    output.append(f"{i+1}{ordinals[i]} competitor was {competitors[i]}")
  else:
    output.append(f"{i+1}{ordinals[3]} competitor was {competitors[i]}")

print(output)