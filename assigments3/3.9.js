function isPrime(n){
  if(n === 1){
    return true;
  }

  for(let i=2; i<n; i++){
    if(n % i === 0){
      return false;
    }
  }
  return true;
}


for(let i=1; i<20; i++){
  console.log(i, isPrime(i));
}