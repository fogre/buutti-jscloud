const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

const divisibleByThree = arr.filter(x => {
  return x % 3 === 0;
});
console.log("divisibleByThree",divisibleByThree);

const multipliedByTwo = arr.map(x => x * 2);
console.log("multipliedByTwo",multipliedByTwo);

const sum = arr.reduce((prev , curr) => {
  return prev + curr;
}, 0);

console.log(sum);