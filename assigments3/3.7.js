

const lottery = () => {
  let numberArr = [];

  while (numberArr.length < 7) {
    const num = Math.floor(Math.random() * 40) + 1;
    if (!numberArr.includes(num)) {
      numberArr = numberArr.concat(num);
    }
  }

  return numberArr;
};

const printLotteryNums = nums => {
  nums.forEach((n, i) => {
    setTimeout(() => {
      printLotteryNums(nums);
      console.log(n);
    }, i * 1000);
  });
};

const nums = lottery();
console.log(nums);
printLotteryNums(nums);

