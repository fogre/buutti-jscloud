import random

arr = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32]

#without using random.shuffle(arr)
random_sorted = sorted(arr, key=lambda x: x * random.randint(-1, 1))
print(random_sorted)
