// const fibonacci = (n, index) => {
//   if (index < 1) {
//     return 0;
//   }

//   if (index === 1) {
//     return 1;
//   }

//   return

// }
const fibonacci = () => {
  const arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  return arr.reduce((acc, cur, index) => {
    if (index === 0) {
      return acc.concat(0);
    }

    if (index === 1) {
      return acc.concat(1);
    }

    return acc.concat(acc[index-2] + acc[index-1]);

  }, []);
};

console.log(fibonacci(10));