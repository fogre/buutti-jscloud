import sys

start = int(sys.argv[1])
end = int(sys.argv[2])

step = 1
if (end < start):
  step = -1

output = [*range(start, end + step, step)]
print(output)