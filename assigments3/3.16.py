import sys

word = sys.argv[1]
length = len(word)

# if word==word[::-1]:
#   return true

for i in range(0, length // 2):
  if word[i] != word[length-i-1]:
    print(f"{word} is not a palindrome")
    sys.exit(0)

print(f"{word} is a palindrome")