module.exports = async function (context, req) {
  context.log("Random num func call");
  const min = req.query.min || 1;
  const max = req.query.max || 10000;
  const isInt = req.query.int || false;

  let randomNumber = Math.random() * (max - min + 1) + min;
  if (isInt) {
    randomNumber = Math.floor(randomNumber);
  }

  context.res = {
    // status: 200, /* Defaults to 200 */
    body: randomNumber
  };
};