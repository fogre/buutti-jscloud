import express from "express";
import path from "path";

const __dirname = path.resolve();
const app = express();
app.use(express.json());

app.get("/api", (req, res) => {
  //no type checking or parsing
  const min = req.query.min || 0;
  const max = req.query.max || 10000;
  const isInt = req.query.int || false;

  try {
    //apparently this is not working as it should, but it's good enough for this exercise.
    let randomNumber = Math.random() * (max - min + 1) + min;
    if (isInt) {
      randomNumber = Math.floor(randomNumber);
    }
    res.json({ randomNumber });
  } catch(e) {
    res.status(400).json({ error: e });
  }
});

app.use("/", express.static(path.join(__dirname, "public")));

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`listening ${PORT}`);
});