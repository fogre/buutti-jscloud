const axios = require("axios");

module.exports = async function (context, myTimer) {
  var timeStamp = new Date().toISOString();

  if (myTimer.isPastDue) {
    context.log("JavaScript is running late!");
  }

  try {
    const getNewFriend = await axios.get("https://api.namefake.com/");
    const { username, name, email_u, email_d } = getNewFriend.data;
    const res = await axios({
      method: "post",
      url: "https://kotirantti-friendlist-app.azurewebsites.net/api",
      data: {
        username,
        name,
        email: `${email_u}@${email_d}`
      }
    });
    console.log(res.data);
  } catch(e) {
    console.log(e);
  }

  context.log("JavaScript timer trigger function ran!", timeStamp);
};