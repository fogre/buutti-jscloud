import express from "express";
import path from "path";

const friendList = [];

const app = express();
app.use(express.json());

//API
app.get("/api", (_req, res) => {
  res.status(200).json(friendList);
});

app.post("/api", (req, res) => {
  const { username, name, email } = req.body;

  if (!username || !name || !email) {
    return res.status(400).json({
      error: "Missing one or many of the required body params: username, name, email"
    });
  }
  const newFriend = { username, name, email };
  friendList.push(newFriend);
  res.status(200).json(newFriend);
});

//Static site
const __dirname = path.resolve();
app.use("/", express.static(path.join(__dirname, "public")));

//Serve
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log("Listening ", PORT);
});