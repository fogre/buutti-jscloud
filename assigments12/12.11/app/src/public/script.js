/* eslint-disable no-undef */
const getFriends = async () => {
  try {
    const friends = await axios.get("/api");
    const friendTableBody = document.querySelector("#friend-table-body");

    friends.data.forEach(f => {
      const trMarkup = `
        <tr>
          <td>${f.username}</td>
          <td>${f.name}</td>
          <td>${f.email}</td>
        </tr>
      `;
      friendTableBody.insertAdjacentHTML("beforeend", trMarkup);
    });
  } catch(e) {
    console.log(e);
  }
};

getFriends();