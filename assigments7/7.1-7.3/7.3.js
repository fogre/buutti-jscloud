const getValue = () => {
  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, _reject) => {
    setTimeout(() => {
      resolve({ value: Math.random() });
    }, Math.random() * 1500);
  });
};

const asyncValue = async () => {
  const val1 = await getValue();
  const val2 = await getValue();
  console.log("asyncValues: ", val1, val2);
};

const promiseValue = () => {
  let val1, val2;
  getValue()
    .then(val => {
      val1 = val;
      return getValue();
    })
    .then(val => {
      val2 = val;
      console.log("promiseValues: ", val1, val2);
    });
};

asyncValue();
promiseValue();