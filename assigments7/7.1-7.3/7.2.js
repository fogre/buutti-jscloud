const timeout = time => new Promise((resolve, reject) => {
  const error = null;

  setTimeout(() => {
    resolve();
  }, time);

  if (error) {
    reject();
  }
});

/*
console.log(3);
timeout(1000)
  .then(() => console.log(2));
timeout(2000)
  .then(() => console.log(1));
timeout(3000)
  .then(() => console.log("GO"));
*/

//in promise chain
console.log(3);
timeout(1000)
  .then(() => {
    console.log(2);
    return timeout(1000);
  })
  .then(() => {
    console.log(1);
    return timeout(1000);
  })
  .then(() => console.log("GO"));


