/* eslint-disable no-undef */
const API_URL = "https://www.omdbapi.com/?apikey=c3a0092f&";
const searchForm = document.querySelector("#searchMovieForm");

const searchMovie = async (title, year) => {
  let queryStr = "";
  if (title) {
    queryStr += `s=${title}`;
  }
  if (year) {
    queryStr += `&y=${year}`;
  }
  try {
    const res = await axios.get(`${API_URL}${queryStr}`);
    return res.data;
  } catch (e) {
    console.log(e);
  }
};

searchForm.addEventListener("submit", async event => {
  event.preventDefault();

  const movies = await searchMovie(
    searchForm.elements["movieName"].value,
    searchForm.elements["movieYear"].value
  );

  searchForm.reset();
  if (movies.Error) {
    document.querySelector("#movieList").innerHTML = "movies not found";
    return;
  }

  let markup = "";
  for (const movie of movies.Search) {
    markup += `
      <article id="${movie.imdbID}">
        <p>Title: ${movie.Title}</p>
        <p>Year: ${movie.Year}</p>
        <a
          href="https://www.imdb.com/title/${movie.imdbID}/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Imdb link
        </a>
        <hr /> 
      </article>
    `;
  }

  document.querySelector("#movieList").innerHTML = markup;
});
