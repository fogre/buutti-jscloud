/* eslint-disable no-undef */
const bacForm = document.querySelector("#bacForm");

bacForm.addEventListener("submit", event => {
  event.preventDefault();
  const elements = bacForm.elements;
  const litres = elements.bacFormDrinkSize.value * elements.bacFormDoses.value / 1000;
  const grams = litres * 8 * elements.bacFormAlcByVolume.value;
  const burning = elements.bacFormWeight.value / 10;
  const gramsLeft = grams - (burning * elements.bacFormDrinkSince.value);
  const result = bacForm.elements.gender.value === "male"
    ? gramsLeft / (elements.bacFormWeight.value * 0.7)
    : gramsLeft / (elements.bacFormWeight.value * 0.6);

  document.querySelector("#bacCalcResult").innerText = `Your BAC is ${result}`;
});

// eslint-disable-next-line no-unused-vars
const resetForm = () => {
  bacForm.reset();
  document.querySelector("#bacCalcResult").innerText = "Not calculated";
};