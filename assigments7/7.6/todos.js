/* eslint-disable no-undef */

const apiFetch = async resourceUrl => {
  const BASE_URL = "https://jsonplaceholder.typicode.com";
  const res = await axios.get(`${BASE_URL}${resourceUrl}`);
  return res.data;
};

const fetchAndParseTodos = async () => {
  const todos = await apiFetch("/todos/");
  const users = await apiFetch("/users/");

  const parsedTodos = todos.map(todo => {
    const user = users[todo.userId - 1];
    return {
      ...todo,
      user: {
        name: user.name,
        username: user.username,
        email: user.email
      }
    };
  });

  console.log(parsedTodos);
};

fetchAndParseTodos();