/* eslint-disable no-undef */

/*
  Some fixes required for this to work properly (like if you want to give negative),
  but this shall be fine enough for this exercise
*/

//Calculation as a string (shown on display)
let calcString = "";
/*
  currentPhase is a tristate "boolean".
  It is used to control the steps in the calculation process.
  Used on button "onclick" functions. Gets values:
  0 = no decimal value has been set
  1 = decial value has been set
  2 = operator value has been set
  depending on which state the calculation process is in
*/
let currentPhase = 2;

const updateDisplay = () => {
  const display = document.querySelector("#display");
  display.innerText = calcString;
};

const handleNumberClick = num => {
  calcString += num;
  if (currentPhase === 2) {
    currentPhase = 0;
  }
  updateDisplay();
};

const handleOperatorClick = operator => {
  if (currentPhase === 0 || currentPhase === 1) {
    calcString += operator;
    currentPhase = 2;
    updateDisplay();
  } else if (currentPhase === 2) {
    calcString = calcString.slice(0, -1) + operator;
    updateDisplay();
  }
};

//number buttons
document.querySelectorAll(".number").forEach(b =>
  b.onclick = () => handleNumberClick(b.innerText)
);

//operator buttons
document.querySelectorAll(".operator").forEach(o =>
  o.onclick = () => handleOperatorClick(o.innerText)
);

//equal button
document.querySelector(".equal").onclick = () => {
  try {
    const result = eval(calcString);
    calcString = result;
    currentPhase = 2;
    updateDisplay();
  } catch (e) {
    calcString = "";
    console.log(e);
    updateDisplay();
  }
};

//decimal button
document.querySelector("#decimal").onclick = () => {
  if (currentPhase === 0) {
    calcString += ".";
    currentPhase = 1;
    updateDisplay();
  }
};

//clear button
document.querySelector("#clear").onclick = () => {
  calcString = "";
  updateDisplay();
};

