export const calculator = (operator, a, b) => {
  switch(operator) {
  case "+":
    return a + b;
  case "-":
    return a - b;
  case "*":
    return a * b;
  case "/":
    return a / b;
  default:
    return `Unknown operator ${operator}!`;
  }
};