import { calculator } from "./calculator";

describe("Calculator test cases:", () => {

  describe("With Multiplication", () => {
    it("Returns correct result", () => {
      expect(calculator("*", 2, 2)).toBe(4);
    });

    it("Returns NaN for String input", () => {
      expect(calculator("*", "asdasd", 2)).toBe(NaN);
    });

    it("Returns NaN for undefined input", () => {
      expect(calculator("*", 2)).toBe(NaN);
    });
  });

  describe("With Division", () => {
    it("Returns correct results", () => {
      expect(calculator("/", 2, 2)).toBe(1);
      expect(calculator("/", 2, -2)).toBe(-1);
    });

    it("Returns Infinity when divided by 0", () => {
      expect(calculator("/", 2, 0)).toBe(Infinity);
    });

    it("Returns NaN for undefined input", () => {
      expect(calculator("/", 2)).toBe(NaN);
    });
  });

  describe("With rest operators so we get 100% coverage", () => {
    it("Tests + and - and unknown operation", () => {
      expect(calculator("+", 2, 2)).toBe(4);
      expect(calculator("-", 2, 2)).toBe(0);
      expect(calculator("?", 2, 2)).toBe("Unknown operator ?!");
    });
  });

});