const [cmdAmount, cmdSourceUnit, cmdTargetUnit] = process.argv.splice(2);

//1 unit amount as a desiliter
export const dlConversions = {
  dl: 1,
  l: 0.1,
  pint: 0.21133,
  oz: 3.38140,
  cup: 0.42267
};

/**
 * @param {Number} amount
 * @param {String} sourceUnit
 * @param {String} targetUnit
 * @returns Converted unit
 * @describe Converts amount first to dl and then to target unit type
 */
export const converter = (amount, sourceUnit, targetUnit) => {
  if (sourceUnit === targetUnit) {
    return amount;
  }

  if (!dlConversions[sourceUnit] || !dlConversions[targetUnit]) {
    return `Invalid unit types: ${sourceUnit}, ${targetUnit}`;
  }

  const asDeciliters = sourceUnit === "dl"
    ? amount * dlConversions[sourceUnit]
    : amount / dlConversions[sourceUnit];

  return asDeciliters * dlConversions[targetUnit];
};

if (process.env.NODE_ENV !== "test" && cmdAmount) {
  console.log(converter(cmdAmount, cmdSourceUnit, cmdTargetUnit));
}