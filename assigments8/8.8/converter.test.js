import { converter, dlConversions } from "./converter";

describe("Converter test cases", () => {
  it("Returns error message with invalid unit type", () => {
    expect(converter(5, "dl", "invalid")).toBe("Invalid unit types: dl, invalid");
  });

  it("Returns correct values for dl to other unit types", () => {
    Object.keys(dlConversions).forEach((unit, i) => {
      expect(converter(i, "dl", unit)).toBe(i * dlConversions[unit]);
    });
  });

  it("Returns correct values for x unit to dl", () => {
    Object.keys(dlConversions).forEach((unit, i) => {
      expect(converter(i, unit, "dl")).toBe(i / dlConversions[unit]);
    });
  });

});