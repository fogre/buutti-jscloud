import express from "express";

const app = express();

app.use("/", express.static("../frontend/dist/"));

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log("listening: ", PORT);
});