import { NavLink, Outlet } from "react-router-dom";
import contacts from "../assets/contactList";

const activeNavStyle = {
  color: "red",
  fontSize: "2rem"
};

const PageLayout = () => {
  return (
    <div>
      <nav style={{ marginBottom: "60px", display: "flex", gap:"10px" }}>
        {contacts.map(c =>
          <NavLink
            key={c.id}
            style={({ isActive }) => isActive ? activeNavStyle : undefined}
            to={`/contact/${c.id}`}
          >
            {c.id}
          </NavLink>
        )}
      </nav>
      <main>
        <Outlet />
      </main>
    </div>
  );
};

export default PageLayout;