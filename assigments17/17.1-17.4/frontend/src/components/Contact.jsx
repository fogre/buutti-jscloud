import { useLoaderData, json } from "react-router-dom";
import contacts from "../assets/contactList.js";

export const loader = ({ params }) => {
  const contact = contacts.find(c => c.id == params.id);
  if (!contact) {
    throw json({}, { status: 404 });
  }
  return contact;
};

const SingleContact = () => {
  const params = useLoaderData();
  return (
    <div style={{ margin: "60px 0" }}>
      <h1>{params.id}</h1>
      <p>{params.email}</p>
      <p>{params.number}</p>
      <p>{params.address}</p>
    </div>
  );
};

export default SingleContact;