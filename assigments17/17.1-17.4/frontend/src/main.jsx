import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import PageLayout from "./components/PageLayout";
import SingleContact, { loader as contactLoader } from "./components/Contact";
import ErrorPage from "./components/ErrorPage";

const routes = createBrowserRouter([
  {
    path: "/",
    element: <PageLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <h1>check this out</h1>
      },
      {
        path: "/contact/:id",
        element: <SingleContact />,
        loader: contactLoader
      },
      {
        path: "/admin",
        element: <h1>Admin</h1>
      }
    ]
  }
]);

// eslint-disable-next-line no-undef
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={routes} />
    <footer>footer</footer>
  </React.StrictMode>,
);
