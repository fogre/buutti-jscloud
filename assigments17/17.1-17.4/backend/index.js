import express from "express";
import path from "path";
import url from "url";

const server = express();

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url));
const distDirectory = path.resolve(currentDirectory, "../frontend/dist/");

server.use("/", express.static(distDirectory));

server.get("*", (_req, res) => {
  res.sendFile("index.html", { root: distDirectory });
});

server.listen(3001);

export default server;