from flask import Flask, render_template
import logging

#local imports
from songs import songs

logging.basicConfig(level=logging.DEBUG)
app = Flask(
  __name__,
  static_folder="dist/assets",
  template_folder="dist"
)

@app.route('/api/songs')
def all_songs():
  return songs

@app.route('/api/songs/<song_id>')
def find_song(song_id):
  try:
    int_id = int(song_id)
    song = next((song for song in songs if song["id"] == int_id), None)
    if song:
      return song
    return f"no song found with id: {song_id}", 400
  except:
    return "invalid id", 404

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template('index.html')

app.run()