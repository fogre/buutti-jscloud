import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import PageLayout from "./components/PageLayout";
import ErrorPage from "./components/ErrorPage";
import SongList  from "./components/SongList";
import SingleSong, { loader as songLoader } from "./components/SingleSong";

const routes = createBrowserRouter([
  {
    path: "/",
    element: <PageLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <SongList />
      },
      {
        path: "/songs/:id",
        element: <SingleSong />,
        loader: songLoader
      },
    ]
  }
]);

// eslint-disable-next-line no-undef
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={routes} />
  </React.StrictMode>,
);
