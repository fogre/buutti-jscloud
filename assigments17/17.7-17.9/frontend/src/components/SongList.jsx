import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const SongList = () => {
  const [songs, setSongs] = useState([]);

  useEffect(() => {
    fetchSongs();
  }, []);

  const fetchSongs = async () => {
    try {
      const songs = await axios.get("/api/songs");
      setSongs(songs.data);
    } catch(e) {
      console.log(e.message);
    }
  };

  return (
    <section>
      <h2>The songs: </h2>
      <ul>
        {songs.map(song =>
          <li key={song.id}>
            <Link to={`/songs/${song.id}`}>
              {song.title}
            </Link>
          </li>
        )}
      </ul>
    </section>
  );
};

export default SongList;