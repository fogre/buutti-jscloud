import axios from "axios";
import { useLoaderData, json } from "react-router-dom";

//In react-router-docs it fetches data here.
export const loader = async ({ params }) => {
  try {
    const songRes = await axios.get(`/api/songs/${params.id}`);
    return songRes.data;
  } catch(e) {
    console.log(e);
    throw json({}, { status: 404 });
  }
};

const SingleSong = () => {
  const song = useLoaderData();

  return (
    <section>
      <h2>{song.title}</h2>
      <p>by: {song.artist}</p>
      <p>lyrics: {song.lyrics}</p>
    </section>
  );
};

export default SingleSong;