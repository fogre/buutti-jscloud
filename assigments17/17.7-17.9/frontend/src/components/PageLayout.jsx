import { Outlet } from "react-router-dom";

const PageLayout = () => {
  return (
    <div>
      <header>
        <h1>Songlist</h1>
      </header>
      <main>
        <Outlet />
      </main>
    </div>
  );
};

export default PageLayout;