import { useRouteError, Link } from "react-router-dom";

const Error404 = () => (
  <>
    <h1>Page not found</h1>
    <p>The resource is not here</p>
    <Link to="/">Go to main page</Link>
  </>
);

const UnknownError = () => (
  <>
    <h1>Unknown error</h1>
    <Link to="/">Go to main page</Link>
  </>
);

const ErrorPage = () => {
  const error = useRouteError();
  console.log(error.status, error.message);
  switch(error.status) {
  case(404):
    return <Error404 />;
  default:
    return <UnknownError />;
  }
};

export default ErrorPage;