import express from "express";
import pg from "pg";

export const pool = new pg.Pool({
  host: "jsc-lecture-17-demo.postgres.database.azure.com",
  port: 5432,
  user: "jscadmin@jsc-lecture-17-demo",
  password: "jscpass1!",
  database: "postgres",
  ssl: true
});

const app = express();

app.get("/message", async (_req, res) => {
  const client = await pool.connect();
  try {
    const result = await client.query("Select * from messages", []);
    res.send(result.rows[0]);
  } catch(error) {
    console.log(error);
    res.status(401).send(error);
  } finally {
    client.release();
  }
});

app.listen(3001, () => {
  console.log(3001);
});

export default app;
