//exercise 1.5

const str = process.argv[2];

if (
  str
  && str === str.trim()
  && str.length <= 20
  && str[0] !== str[0].toUpperCase()
) {
  console.log(str);
} else {
  console.log(`Please insert valid cmd line string with:
    - no whitespace in the beginning or end
    - max length 20
    - starts with lowercase character
  `);
}