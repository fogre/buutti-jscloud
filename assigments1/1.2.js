/*
  assigment 1.2.
  Optional cmd variables A and B (positive integers)
*/
const a = parseInt(process.argv[2]) || 1;
const b = parseInt(process.argv[3]) || 1;

console.log("Sum: ", a + b);
console.log("Difference: ", a - b);
console.log("Fraction: ", a / b);
console.log("Product: ", a * b);
console.log("Exponentation: ", Math.pow(a, b));
console.log("Modulo: ", a % b);
console.log("Average: ", (a + b) / 2);

