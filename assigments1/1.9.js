const length = process.argv[2];

if (isNaN(length)) {
  console.log(";(");
  process.exit(1);
}

const area = length * length;
console.log(area);