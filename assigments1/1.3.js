/*
  assigment 1.3
  Optional cmd variables str1 and str2 (strings)
*/
const str1 = process.argv[2] || "acra";
const str2 = process.argv[3] || "bacabra";

const str_sum = str1 + str2;
console.log(str_sum);

const str_avg = str_sum.length / 2;

if (str1.length < str_avg) console.log(str1);
if (str2.length < str_avg) console.log(str2);