import { jest } from "@jest/globals";
import request from "supertest";
import server from "../index.js";
import { pool } from "../db/db";

const mockPool = mockRes => jest.fn(() => {
  return {
    query: () => mockRes,
    release: () => null
  };
});

describe("When using products /GET", () => {
  const mockRes = {
    rows: [
      { id: 1, name: "silly product", price: 10 },
      { id: 2, name: "another product", price: 2 }
    ]
  };

  beforeEach(() => {
    pool.connect = mockPool(mockRes);
  });

  it("Returns 200 with all products for /", async () => {
    const res = await request(server).get("/products");
    expect(res.status).toBe(200);
    expect(res.body).toEqual(mockRes.rows);
  });
});

describe("When using products /POST", () => {
  const mockRes = { id: 3, name: "newProd", price: 10 };

  beforeEach(() => {
    pool.connect = mockPool(mockRes);
  });

  it("Returns 200 and creates new product for /", async () => {
    const res = await request(server)
      .post("/products")
      .send([ mockRes.name, mockRes.price ]);
    expect(res.status).toBe(200);
    expect(res.body).toEqual(mockRes);
  });
});