import express from "express";
import { initProductTable } from "./db/db";
import productDao from "./db/productDao";

initProductTable();

const server = express();
server.use(express.json());

server.get("/products", async (_req, res) => {
  const daoRes = await productDao.getProducts();
  res.send(daoRes.rows);
});

server.post("/products", async (req, res) => {
  const { name, price } = req.body;
  const daoRes = await productDao.addNewProduct([ name, price ]);
  res.send(daoRes);
});

server.put("/products/:id", async (req, res) => {
  const { name, price } = req.body;
  const productsInDb = await productDao.getProduct(req.params.id);
  if (!productsInDb.rowCount) return res.status(400).send({
    userInputError: "No product found with id: " + req.params.id
  });

  const dbProduct = productsInDb.rows[0];
  const updatedProduct = {
    id: dbProduct.id,
    name: name || dbProduct.name,
    price: price || dbProduct.price
  };
  const daoRes = await productDao.updateProduct(updatedProduct);
  res.send(daoRes.rows);
});

export default server;
