export const createProductTable = `
  CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL,
    "name" varchar(100) NOT NULL,
    "price" INTEGER NOT NULL,
    PRIMARY KEY("id")
  );
`;

const listAllProducts = "SELECT id, name, price FROM products;";

const getProduct = "SELECT id, name, price FROM products WHERE id = $1;";

const addNewProduct = "INSERT INTO products (name, price) VALUES ($1, $2)";

const updateProduct = `
  UPDATE products
  SET name = $2, price = $3
  WHERE id = $1;
`;

export default {
  addNewProduct,
  createProductTable,
  getProduct,
  listAllProducts,
  updateProduct
};