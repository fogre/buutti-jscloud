import queries from "./productQueries";
import { executeQuery } from "./db";

const getProducts = async () => {
  const dbRes = await executeQuery(queries.listAllProducts);
  return dbRes;
};

const getProduct = async id => {
  const dbRes = await executeQuery(queries.getProduct, id);
  return dbRes;
};

const addNewProduct = async (name, price) => {
  const dbRes = await executeQuery(queries.addNewProduct, [name, price]);
  return dbRes;
};

const updateProduct = async product => {
  const dbRes = await executeQuery(queries.updateProduct, Object.values(product));
  return dbRes;
};

export default {
  addNewProduct,
  getProduct,
  getProducts,
  updateProduct
};