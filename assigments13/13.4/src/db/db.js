import pg from "pg";
import { createProductTable } from "./productQueries";

const isProd = process.env.NODE_ENV === "production";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, NODE_ENV } = process.env;

export const pool = new pg.Pool({
  host: PG_HOST,
  port: PG_PORT,
  user: PG_USERNAME,
  password: PG_PASSWORD,
  database: PG_DATABASE,
  ssl: isProd
});

export const executeQuery = async (query, params) => {
  const client = await pool.connect();
  try {
    const result = client.query(query, params);
    return result;
  } catch(error) {
    console.log(error.stack);
    error.name = "dbError";
    throw error;
  } finally {
    client.release();
  }
};

export const initProductTable = async () => {
  if (NODE_ENV === "test") return null;
  await executeQuery(createProductTable);
};