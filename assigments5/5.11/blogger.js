//blog index "id" that gets incremented by each new blog
let blogIndex = 0;
// eslint-disable-next-line no-undef
const form = document.querySelector("#formAddBlog");

const blogMarkupGenerator = (blog, i) => `
  <article id=${i}Blogpost>
    <h2>${blog.name}</h2>
    <p>${blog.post}</p>
    <button onClick=deleteBlog(${i})>delete</button>
  </article>
`;

// eslint-disable-next-line no-unused-vars
const deleteBlog = index => {
  // eslint-disable-next-line no-undef
  document.getElementById(`${index}Blogpost`).remove();
};

// eslint-disable-next-line no-unused-vars
const changeFormVisibility = () => {
  form.style.display = form.style.display === "none"
    ? "block"
    : "none";
};

form.addEventListener("submit", event => {
  event.preventDefault();
  const blog = {
    name: form.elements["addBlogName"].value,
    post: form.elements["addBlogPostText"].value
  };
  // eslint-disable-next-line no-undef
  document.querySelector("#blogPosts").innerHTML += blogMarkupGenerator(blog, blogIndex);
  blogIndex++;
  form.reset();
});


